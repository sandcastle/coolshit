﻿using System;
using System.Threading.Tasks;
using Cli.Actions;
using CommandLine;

namespace Cli
{
    class Program
    {
        static int Main(string[] args) =>
            Parser.Default.ParseArguments<AddOptions>(args)
            .MapResult(
                (AddOptions opts) => AddOptions.Run(opts).GetAwaiter().GetResult(),
                errs => 1);
    }
}