using System.Drawing;
using Colorful;
using CommandLine;
using Console = Colorful.Console;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using shortid;

namespace Cli.Actions
{
    [Verb("add", HelpText = "Add a new article for editing.")]
    public class AddOptions
    {
        [Option('n', "name", Default = true, Required = true, HelpText = "The name of the article.")]
        public string Name { get; set; }

        [Option('t', "tags", HelpText = "A list of arguments")]
        public string Tags { get; set; }

        public static async Task<int> Run(AddOptions opts)
        {
            var name = CreateName(opts);

            var path = Path.Combine(Directory.GetCurrentDirectory(), "..", "docs", name);
            if (File.Exists(path))
            {
                Console.WriteLineFormatted("The article {0} already exists", Color.Gray,
                    new []
                    {
                        new Formatter(name, Color.Red)
                    });
                return 1;
            }

            using(var writer = File.CreateText(path))
            {
                await writer.WriteLineAsync("---");
                await writer.WriteLineAsync($"title: {opts.Name}");
                await writer.WriteLineAsync($"date: {DateTime.Now.ToString("yyyy-MM-dd")}");
                await writer.WriteLineAsync($"tags: {opts.Tags}");
                await writer.WriteLineAsync("---");
                await writer.WriteLineAsync("");
            }

            OpenFile(path);

            return 0;
        }

        static void OpenFile(string path)
        {
            Process.Start(new ProcessStartInfo("code", path));
        }

        static string CreateName(AddOptions opts)
        {
            var id = ShortId.Generate(8)
                .ToLowerInvariant();

            var name = opts.Name
                .Replace(" ", "_")
                .Trim()
                .ToLowerInvariant();

            return $"{id}__{name}.md";
        }
    }
}
