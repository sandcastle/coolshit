---
title: Replacing strings in postgres
date: 2018-11-01
tags: sql, postgres
---

Sometimes, you want to search and replace a string with a new one such as replacing outdated phone numbers, broken URLs, and spelling mistakes.

You can use the `replace(source, old_text, new_text)` funtion to replace strings.

```sql
> select replace('brian@hotmail.com', 'hotmail.com', 'anon.com');

brian@anon.com
```

The `replace()` function accepts three arguments:
- `source` is a string where you want to replace.
- `old_text` is the text that you want to search and replace; if the `old_text` appears multiple times in the string, all will be replaced.
- `new_text` is the new text that will replace the old text (`old_text`).

To update values in a table, you can use `replace` in an udpate statement:


```sql
UPDATE user_emails
SET email = replace(email, 'hotmail.com', 'anon.com')
WHERE condition
```

Postgres also has some other great string functions like `regexp_replace` which enables the use of regex for advanced scenarios. You can find out more about string functions here:

[https://www.postgresql.org/docs/current/static/functions-string.html]()
