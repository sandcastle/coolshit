---
title: Migrate pods between kubernetes node pools
date: 2018-11-03
tags: kubernetes, gcp, docker
---

You will first want to create a new node pool, before cordoning any nodes (making them
unschedulable).

Once you are ready, check the pool (e.g. `default-pool`) you are going to migrate is correct:

```sh
kubectl get nodes -l cloud.google.com/gke-nodepool=default-pool
```

Cordon the nodes, so that no new pods can be scheduled on them:

```sh
for node in $(kubectl get nodes -l cloud.google.com/gke-nodepool=default-pool -o=name); do
  kubectl cordon "$node";
done
```

Check the status of the nodes:

```sh
kubectl get nodes
```

They should show as `SchedulingDisabled` in the status:

```sh
NAME                                                STATUS                     AGE       VERSION
gke-migration-tutorial-default-pool-56e3af9a-059q   Ready,SchedulingDisabled   1h        v1.5.7
gke-migration-tutorial-default-pool-56e3af9a-0ng4   Ready,SchedulingDisabled   1h        v1.5.7
gke-migration-tutorial-default-pool-56e3af9a-k6jm   Ready,SchedulingDisabled   1h        v1.5.7
```

Finally, force the current pods to be drained from the cordoned nodes, so they are recheduled onto
the new node pool:

```sh
for node in $(kubectl get nodes -l cloud.google.com/gke-nodepool=default-pool -o=name); do
  kubectl drain --force --ignore-daemonsets --delete-local-data --grace-period=10 "$node";
done
```

Finally, you can delete the old node pool if no longer required.

```sh
gcloud container node-pools delete default-pool --cluster migration-tutorial
```

Magic!
