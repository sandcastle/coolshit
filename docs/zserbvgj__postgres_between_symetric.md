---
title: Postgres Symmetric Between operator
date: 2018-10-16
tags: sql, postgres
---

Postgres has a `between` operator to test if a value fits within a range of two values for
types like numbers, timestamps, etc.

```sql
select * from generate_series(1,10) as numbers(a)
    where numbers.a between 3 and 5;
```

```sh
 a
---
 3
 4
 5
```

However, if the first number is larger (5) than the second (3), not results will be returned:

```sql
select * from generate_series(1,10) as numbers(a)
    where numbers.a between 5 and 3;
```

```sh
 a
---
```

Using `between symmetric` will avoid this issue:

```sql
> select * from generate_series(1,10) as numbers(a)
    where numbers.a between symmetric 5 and 3;
```

```sh
 a
---
 3
 4
 5
```
